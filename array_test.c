/*
This module tests array handling, dynamic memory allocation and pointers.
Parameters are given when program is started (> function key1 value key2 value etc.)

gcc compiler used 
	gcc compiler from minigw  https://sourceforge.net/projects/mingw-w64/
	gcc array_test.c -Wall -o array_test.exe

How to start program: 
	- Command promt-window
			array_test key1 4 key2 333 key3 7 key4 6
	- Windows PowerShell-window
			.\array_test key1 4 key2 333 key3 7 key4 6

Case 1:
Start program with parameters (see  "How to start program")
Reserve enought space from dynamic memory for two dimensional array
Copy parameters to two dimensional array
Use subfunction:
	to print out nbr of pairs as parameters
	to print out array (pointer to array + nbr of pairs as parameters)

Case 2:
Use an other subfunction to sort array according the "value" to ascending order (from small numbers to big number)
using qsort()-function
Print out sorted array 

Note1:	Free dynamic memory in main function
Note 2:	argv[0] shows execution module name (in this case "array_test")
Note 3: For clarification reasons extense error checking not included
*/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct
{
char* key;
int value;
} KeyValuePair;

void unsorted_array(KeyValuePair *table_ptr, int nbr_of_items);
void sorted_array(KeyValuePair *table_ptr, int nbr_of_items);


int main(int argc, char *argv[])
{
	int nbr_of_items; //zeroing not needed
	KeyValuePair *tableptr;
	KeyValuePair *apu_ptr;

	nbr_of_items = (argc-1) / 2;	// nbr of pairs "Key + nbr"

	tableptr = malloc(nbr_of_items * sizeof(KeyValuePair));		// mem allocation
	if (tableptr == NULL) {
		printf("Couldn't able to allocate requested memory\n");
		return EXIT_FAILURE;
	}

	/* //testing: just printing given parameters
	//printf("%s\n", argv[0]); // prints name of exe file
	for (int i = 1; i<argc+1; i++) {
		printf("%s\n", argv[i]);
	}
	printf("\n");
	*/
	apu_ptr = tableptr; //apu_ptr needed because tableptr must have original address for free-function

	printf("Nbr of pairs: %d\n", nbr_of_items);
	printf("Given parameters:\n");

	// parameters copied to table
	for (int i = 1; i < (nbr_of_items+1); i++)	{
		apu_ptr->key = argv[(2*i)-1];	// parameters 1,3,5,7 etc
		printf("%s\t", apu_ptr->key);

		apu_ptr->value = atoi(argv[(2*i)]); //atoi converts numbers from ascii format to numbers
		printf("%d\t", apu_ptr->value);		// parameters 2,4,6,8 etc
		apu_ptr++;
	}
	printf("\n");

	//Case 1:
	unsorted_array(tableptr, nbr_of_items);					// print unsorted array
	
	//Case 2:
	sorted_array(tableptr, nbr_of_items);					// print sorted array

	free(tableptr);											// free mem

	return 0;
}

// Case 1: print unsorted array
void unsorted_array(KeyValuePair *table_ptr, int nbr_of_items) {
	
	printf("\nUnsorted array:\n");
	printf("Nbr of pairs %d\n", nbr_of_items);
	
	for (int i = 0; i < nbr_of_items; i++)
	{
		printf("%s\t", table_ptr->key);
		printf("%d\n", table_ptr->value);
		table_ptr++;
	}

	return;
}

// Case 2: print sorted array
void sorted_array(KeyValuePair *table_ptr, int nbr_of_items) {

	// needed in qsort-function
	int comparator(const void *p, const void *q)
	{
		int l = ((KeyValuePair *)p)->value;
		int r = ((KeyValuePair *)q)->value;
		return (l - r);
	}

	printf("\nSorted array by value in array:\n");
	printf("Nbr of pairs %d\n", nbr_of_items);
		
	qsort((void*)table_ptr, nbr_of_items, sizeof(KeyValuePair), comparator);	// array sorted

	for (int i = 0; i < nbr_of_items; i++)
	{
		printf("%s\t", table_ptr->key);
		printf("%d\n", table_ptr++->value);
	}

	return;
}