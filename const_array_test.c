/*
This module tests array handling, dynamic memory allocation and pointers.
Program is started without parameters

gcc compiler used from minigw  https://sourceforge.net/projects/mingw-w64/
gcc const_array_test.c -Wall -o const_array_test.exe

How to start program:
- Command promt-window
		const_array_test
- Windows PowerShell-window
		.\const_array_test

Case 1:
Start program without parameters (see  "How to start program")
Reserve enought space from dynamic memory for two dimensional array
Copy array from const area to two dimensional array
Use subfunction:
to print out nbr of pairs as parameters
to print out array (pointer to array + nbr of pairs as parameters)

Case 2:
Use an other subfunction to sort array according the "value" to ascending order (from small numbers to big number)
using qsort()-function
Print out sorted array

Note 1:	Free dynamic memory in main function
Note 2: For clarification reasons extensive error checking not included
*/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct
{
char* key;
int value;
} KeyValuePair;

void unsorted_array(KeyValuePair *table_ptr, int nbr_of_items);
void sorted_array(KeyValuePair *table_ptr, int nbr_of_items);

int main(int argc, char** argv)
{
	const KeyValuePair kvp_array[] = { {"key1", 3}, {"key2", 55}, {"key3",333},{ "key4", 5 },{ "key5", 12 } };
	int nbr_of_items; //zeroing not needed
	KeyValuePair *tableptr;

	nbr_of_items=sizeof(kvp_array)/sizeof(KeyValuePair);	// nbr of item pairs in kvp_array
	//printf("Size %d\n", sizeof(kvp_array));				// testing: size of array

	tableptr = malloc(sizeof(kvp_array));					// mem allocation
	if (tableptr == NULL){
		printf("Couldn't able to allocate requested memory\n");
		return EXIT_FAILURE;
	}

	memcpy((char *)tableptr, (char *) kvp_array, (sizeof(kvp_array))); //copy from const area data
	//void *memcpy(void *str1, const void *str2, size_t n)

	//Case 1:
	unsorted_array(tableptr, nbr_of_items);					// print unsorted array
	
	//Case 2:
	sorted_array(tableptr, nbr_of_items);					// print sorted array

	free(tableptr);											// free mem

	return 0;
}

// Case 1: print unsorted array
void unsorted_array(KeyValuePair *table_ptr, int nbr_of_items) {
	
	printf("\nUnsorted array:\n");
	printf("Nbr of pairs %d\n", nbr_of_items);

	for (int i = 0; i < nbr_of_items; i++)
	{
		printf("%s\t", table_ptr->key);
		printf("%d\n", table_ptr++->value);
	}

	return;
}

// Case 2: print sorted array
void sorted_array(KeyValuePair *table_ptr, int nbr_of_items) {

	// needed in qsort-function
	int comparator(const void *p, const void *q)
	{
		int l = ((KeyValuePair *)p)->value;
		int r = ((KeyValuePair *)q)->value;
		return (l - r);
	}

	printf("\nSorted array by value in array:\n");
	printf("Nbr of pairs %d\n", nbr_of_items);
		
	qsort((void*)table_ptr, nbr_of_items, sizeof(KeyValuePair), comparator);	// array sorted

	for (int i = 0; i < nbr_of_items; i++)
	{
		printf("%s\t", table_ptr->key);
		printf("%d\n", table_ptr++->value);
	}

	return;
}