/*
This module show how to deliver parameters from Windows PowerShell/Command promt- window to C-program.
Parameters are given when program is started (> function para1 para2 para3 etc.)

gcc compiler used
gcc compiler from minigw  https://sourceforge.net/projects/mingw-w64/
gcc para_test.c -Wall -o para_test.exe

How to start program:
- Command promt-window
para_test para1 para2 para3 para4
- Windows PowerShell-window
.\para_test para1 para2 para3 para4

Program prints parameters.
Note! Parameter argv[0] shows module name.
*/
#include<stdio.h>

//argc contains nbr of parameters
//argv[] contains array of parameters
int main(int argc, char *argv[]){

	printf("Nbr of parameters:%d\n", argc);

	for (int i = 0; i<argc; i++){
		printf("Parameter%d: %s\n", i, argv[i]);
	}

	return 0;
}
